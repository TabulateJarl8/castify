from os import environ as env

import dotenv
import requests

from flask import Flask, render_template, request
from jinja2 import TemplateNotFound

# load dotenv file
ENV_FILE = dotenv.find_dotenv()
if ENV_FILE:
	dotenv.load_dotenv(ENV_FILE)

# initialize application
app = Flask(
	__name__,
	template_folder='templates/'
)
app.secret_key = env.get("APP_SECRET_KEY")

def send_api_request(route, **kwargs):
	api_url = f'https://api.themoviedb.org/3/{route.strip("/")}?api_key={env.get("TMDB_API_KEY")}&language=en-US&page=1'
	for key, value in kwargs.items():
		api_url += f'&{key}={value}'

	return requests.get(api_url)

# routes
@app.route('/', defaults={'page': 'index'})
@app.route('/<page>/')
def home(page):
	try:
		return render_template(f'{page}.html')
	except TemplateNotFound:
		return 'Page not found', 404

@app.route('/search/', methods=['GET'])
def media_search():
	query = request.args.get('query')
	type = request.args.get('type')

	if type.lower() not in ('tv', 'movie'):
		return 'Invalid type', 400

	return send_api_request(f'/search/{type}', query=query).json()

@app.route('/comparecast')
def comparecast():
	# process/validate types
	if 'type1' not in request.args or 'type2' not in request.args:
		return 'Missing type1 or type2', 400
	type1 = request.args.get('type1').lower()
	type2 = request.args.get('type2').lower()

	if type1 not in ('tv', 'movie'):
		return 'Invalid type', 400
	if type2 not in ('tv', 'movie'):
		return 'Invalid type', 400

	# process ID 1
	if 'id1' in request.args:
		id1 = request.args.get('id1')
	elif 'search1' in request.args:
		response = send_api_request(f'/search/{type1}', query=request.args.get('search1')).json()
		if not response['results']:
			return f'No results for `{request.args.get("search1")}`', 400

		id1 = response['results'][0]['id']
	else:
		return 'Missing id1 or search1', 400

	# process ID 2
	if 'id2' in request.args:
		id2 = request.args.get('id2')
	elif 'search2' in request.args:
		response = send_api_request(f'/search/{type2}', query=request.args.get('search2')).json()
		if not response['results']:
			return f'No results for `{request.args.get("search2")}`', 400

		id2 = response['results'][0]['id']
	else:
		return 'Missing id2 or search2', 400

	id1 = int(id1)
	id2 = int(id2)

	# get media info
	media_metadata_1 = send_api_request(f'/{type1}/{id1}').json()
	media_metadata_2 = send_api_request(f'/{type2}/{id2}').json()

	filmname1 = media_metadata_1['original_name'] if 'original_name' in media_metadata_1 else media_metadata_1['original_title']
	filmname2 = media_metadata_2['original_name'] if 'original_name' in media_metadata_2 else media_metadata_2['original_title']
	photo1 = f'https://image.tmdb.org/t/p/original/{media_metadata_1["backdrop_path"]}'
	photo2 = f'https://image.tmdb.org/t/p/original/{media_metadata_2["backdrop_path"]}'

	# get cast info
	credits_type_1 = 'aggregate_credits' if type1 == 'tv' else 'credits'
	credits_type_2 = 'aggregate_credits' if type2 == 'tv' else 'credits'

	cast1 = send_api_request(f'/{type1}/{id1}/{credits_type_1}').json()['cast']
	cast2 = send_api_request(f'/{type2}/{id2}/{credits_type_2}').json()['cast']

	# find intersecting cast members
	cast_ids_1 = {actor['id'] for actor in cast1}
	cast_ids_2 = {actor['id'] for actor in cast2}

	cast_intersection_set = cast_ids_1.intersection(cast_ids_2)
	cast_intersecion = {
		actor['id']: {
			'name': actor['name'],
			'picture': actor['profile_path'],
			'roles_1': actor['roles'] if 'roles' in actor else [{'character': actor['character']}]
		} for actor in cast1 if actor['id'] in cast_intersection_set
	}

	for actor in cast2:
		if actor['id'] in cast_intersection_set:
			cast_intersecion[actor['id']]['roles_2'] = actor['roles'] if 'roles' in actor else [{'character': actor['character']}]

	return render_template(
		'castcompare.html',
		filmname1=filmname1,
		filmname2=filmname2,
		photo1=photo1,
		photo2=photo2,
		cast_intersecion=cast_intersecion
	)
