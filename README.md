<img src='logos/castify_logo_transparent.png' width="50%" />

## Find shared cast of your favorite movies and TV shows

Castify allows you to easily find out if two different  movies or TV shows share any cast. Using a Flask backend, Castify searches the [TMBD](https://www.themoviedb.org/) database to get the latest information on movies and TV. Castify is still in beta, so if you encounter any issues, please start an issue on the GitLab page.

## Usage/self-host
Castify is currently in beta. You can visit castify at [](). You can also self-host Castify very easily. First, clone the repository and cd into the directory. Now, create a virtual environment with `python3 -m venv venv`. Activate the virtual environment and install the requirements with `pip3 install -r requirements.txt`. Now, create a `.env` file. This file will need two values, the first being `APP_SECRET_KEY`. You can create a secret key with `openssl rand -hex 32`. The second value you will need is the `TMDB_API_KEY` value. You can get an API key by following the instructions [here](https://developers.themoviedb.org/3/getting-started/introduction). Here is our example file:
```
APP_SECRET_KEY=1234567890123456789012345678901234567890123456789012345678901234
TMDB_API_KEY=12345678901234567890123456789012
```

Once this step is done, you should be able to start the server with `flask run`.


## Is this project going to be mobile friendly?
I plan to make this project mobile friendly at some point in the future.
